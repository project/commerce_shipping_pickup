<?php

declare(strict_types = 1);

namespace Drupal\commerce_shipping_pickup;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileInterface;

/**
 * An example dealers implementation to override.
 */
class PickupProfileMapper implements PickupProfileMapperInterface {

  use StringTranslationTrait;
  /**
   * Some examplary dealers.
   *
   * @var array
   */
  protected $dealers = [
    [
      'country_code' => "CH",
      'locality' => "Zürich",
      'postal_code' => "8000",
      'address_line1' => "Strasse 1",
      'organization' => "Dealer 1",
    ],
    [
      'country_code' => "CH",
      'locality' => "Bern",
      'postal_code' => "5000",
      'address_line1' => "Strasse 2",
      'organization' => "Dealer 2",
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile): void {
    $id = $profile->getData('pickup_location_id');
    $profile->set('address', $this->dealers[$id]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    return [
      '#type' => 'select',
      '#title' => $this->t('Select a dealer:'),
      '#default_value' => $profile->getData('pickup_location_id'),
      '#options' => array_map(function ($address) {
        return $address['organization'];
      }, $this->dealers),
    ];
  }

}
