<?php

declare(strict_types=1);

namespace Drupal\commerce_shipping_pickup\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Provides the free shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup",
 *   label = @Translation("Pickup shipping"),
 * )
 */
final class PickupShipping extends ShippingMethodBase {

  /**
   * Constructs a new free shipping object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $package_type_manager,
      $workflow_manager
    );
    $this->services['default'] = new ShippingService(
      'pickup',
      'Pickup shipping'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'services' => ['default'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $rates = [];
    $order = $shipment->getOrder();
    if ($order && $order->getTotalPrice()) {
      $amount = new Price(
        '0',
        $order->getTotalPrice()->getCurrencyCode()
      );
      $rates[] = new ShippingRate(
        [
          'shipping_method_id' => $this->parentEntity->id(),
          'service' => $this->services['default'],
          'amount' => $amount,
        ]
      );
    }
    return $rates;
  }

}
