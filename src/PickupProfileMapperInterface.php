<?php

namespace Drupal\commerce_shipping_pickup;

use Drupal\profile\Entity\ProfileInterface;

/**
 * Interface of the pickup mapper.
 */
interface PickupProfileMapperInterface {

  /**
   * Populates the profile.
   *
   * @param \Drupal\linkit\ProfileInterface $profile
   *   The profile to populate.
   */
  public function populateProfile(ProfileInterface $profile): void;

  /**
   * Defines the type of input field the pane should provide.
   *
   * @param \Drupal\linkit\ProfileInterface $profile
   *   The profile as context.
   */
  public function buildFormElement(ProfileInterface $profile): array;

}
