# Commerce shipping pickup
Provides shipping method for dealer pickup until the feature is in commerce core.

## Installation
Install the module through composer:
```
composer require drupal/commerce_shipping_pickup
```
Enable the module:
```
drupal en commerce_shipping_pickup
```
Add the new pickup_capable_shipping_information with label "Shipping information" to your checkout flow or use the pickup checkout flow.

Add a new shipping method from plugin pickup. Make sure its not the first selectable method. (known issue)

## Known issues
If the pickup shipping method is the first to select, the wrong inline form is rendered. 
